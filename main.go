package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

const PROGRAM_NAME = "csv-dumper"

func fatal_err(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func quote_if_comma(input string) string {
	if strings.Contains(input, ",") {
		return "\"" + input + "\""
	} else {
		return input
	}
}

// func fn_map[T any, V any](fn func(T) V, input []T) []V {
// 	output := make([]V, len(input))

// 	for i, e := range input {
// 		output[i] = fn(e)
// 	}

// 	return output
// }

func main() {
	opts := parse_args()

	var headers []string
	rows := make(chan []string)

	switch {
	case opts.IsLocDescs:
		go process_json[LocationDescription](opts.Items.Url, rows)
		headers = LocationDescriptionHeaders()
	case opts.IsMultipliers:
		go process_json[MeterMultiplier](opts.Items.Url, rows)
		headers = MeterMultiplierHeaders()
	case opts.IsServTypes:
		go process_json[ServiceType](opts.Items.Url, rows)
		headers = ServiceTypeHeaders()
	}

	if opts.Items.File == "-" {
		fmt.Println(strings.Join(headers, ","))
		for e := range rows {
			line := strings.Join(e, ",")
			fmt.Println(line)
		}
	} else {
		f, err := os.Create(opts.Items.File)
		fatal_err(err)
		defer f.Close()

		f.WriteString(strings.Join(headers, ","))
		f.WriteString("\n")
		for e := range rows {
			line := strings.Join(e, ",")
			f.WriteString(line)
			f.WriteString("\n")
		}
		f.Sync()
	}
}
