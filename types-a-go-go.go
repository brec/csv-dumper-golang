package main

import (
	"strconv"
	"strings"
)

type ContainerType interface {
	to_string_array() []string
}

type Address struct {
	StreetNumber string
	StreetName   string
	StreetType   string
	Unit         string
	City         string
	State        string
	Zip          string
}

func (item Address) to_string_array() []string {
	output := []string{}

	output = append(output, quote_if_comma(item.StreetNumber))
	output = append(output, quote_if_comma(item.StreetName))
	output = append(output, quote_if_comma(item.StreetType))
	output = append(output, quote_if_comma(item.Unit))
	output = append(output, quote_if_comma(item.City))
	output = append(output, quote_if_comma(item.State))
	output = append(output, quote_if_comma(item.Zip))

	return output
}

type LocationDescription struct {
	MapNumber   string `json:"mapNumber"`
	Description string `json:"description"`
}

func (item LocationDescription) to_string_array() []string {
	output := []string{}

	output = append(output, item.MapNumber)
	output = append(output, item.Description)

	return output
}

type MeterMultiplier struct {
	MeterNumber int `json:"meterNumber"`
	Multiplier  int `json:"multiplier"`
}

func (item MeterMultiplier) to_string_array() []string {
	output := []string{}

	multiplier := item.Multiplier
	if multiplier == 0 {
		multiplier = 1
	}

	output = append(output, strconv.Itoa(item.MeterNumber))
	output = append(output, strconv.Itoa(multiplier))

	return output
}

type ServiceType struct {
	MapNumber      string
	ServiceType    string
	RateCode       string
	AccountNumber  uint64
	DisconnectDate string
	AccountStatus  string
	Phone          string
	ReadingDate    string
	MemberName     string
	Address        Address
	MeterNumber    uint64
}

func (item ServiceType) to_string_array() []string {
	output := []string{}

	var disconnectDate string
	if item.DisconnectDate != "" {
		disconnectDate = item.DisconnectDate[:10]
	} else {
		disconnectDate = ""
	}

	var readingDate string
	if item.ReadingDate != "" {
		readingDate = item.ReadingDate[:10]
	} else {
		readingDate = ""
	}

	output = append(output, quote_if_comma(item.MapNumber))
	output = append(output, quote_if_comma(item.ServiceType))
	output = append(output, quote_if_comma(item.RateCode))
	output = append(output, strconv.FormatUint(item.AccountNumber, 10))
	output = append(output, disconnectDate)
	output = append(output, quote_if_comma(item.AccountStatus))
	output = append(output, quote_if_comma(item.Phone))
	output = append(output, readingDate)
	output = append(output, quote_if_comma(item.MemberName))
	output = append(output, strings.Join(item.Address.to_string_array(), ","))
	output = append(output, strconv.FormatUint(item.MeterNumber, 10))

	return output
}

func LocationDescriptionHeaders() []string {
	return []string{"newid", "description"}
}

func MeterMultiplierHeaders() []string {
	return []string{"meterno", "multiplier"}
}

func ServiceTypeHeaders() []string {
	return []string{
		"map_number",
		"service_type",
		"rate_code",
		"account_number",
		"disconnect_date",
		"account_status",
		"phone_number",
		"reading_date",
		"customer_name",
		"street_number",
		"street_name",
		"street_type",
		"street_unit",
		"address_city",
		"address_state",
		"address_zip",
		"meter_number",
	}
}
